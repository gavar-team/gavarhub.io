import autoprefixer from "autoprefixer";
import del from "del";
import gulp, { parallel, series } from "gulp";
import postcss from "gulp-postcss";
import sass from "gulp-sass";
import ts from "gulp-typescript";
import Handlebars from "handlebars";
import HandlebarsHelpers from "handlebars-helpers";

import path from "path";
import vynil from "vinyl";
import { TaskCallback } from "./core";
import { TaskInfo } from "./core/types";
import { beautify, render, resources, responsify, rollupify } from "./tools";

const layouts = require("handlebars-layouts");
const registrar = require("handlebars-registrar");
const HandlebarsIntl = require("handlebars-intl");
const minifier = require("gulp-minifier");

const outDirectory = "./gh-pages";
const production = process.env.NODE_ENV === "production" || process.argv.includes("-p");

/** Clean previous compilation files. */
function clean(done: TaskCallback) {
  del([`${outDirectory}/**`, "./temp", `!**/.git`]).then(x => done(void 0, x), done);
}
gulp.task(clean);

/** Compile page styles */
function style(): NodeJS.ReadWriteStream {
  return gulp.src("./src/app/**/*.+(css|scss)")
    .pipe(sass({
      includePaths: [
        "./src",
      ],
    }))
    .pipe(postcss([autoprefixer()]))
    .pipe(beautify.css())
    .pipe(gulp.dest(outDirectory));
}
gulp.task(style);

/** Render page templates. */
function pages() {

  // BUG: handlebars-layouts doesnt work if creating instance of handlebars using Handlebars.create()
  const handlebars = Handlebars;

  // handlebars setup
  handlebars.registerHelper(layouts(handlebars));
  HandlebarsIntl.registerWith(handlebars);
  (HandlebarsHelpers as any).comparison({handlebars});
  (HandlebarsHelpers as any).date({handlebars});
  (HandlebarsHelpers as any).math({handlebars});

  registrar(handlebars, {
    cwd: "./src",
    partials: [
      "pages/**/*.hbs",
      "layouts/**/*.hbs",
      "components/**/*.hbs",
    ],
    bustCache: true,
    parsePartialName: function (file: vynil) {
      let name = file.path;

      // remove extension / category name
      const index = name.indexOf(".");
      if (index > 0)
        name = name.substring(0, index);

      // use 'node style' by removing '.../index'
      if (name.endsWith("/index"))
        name = name.slice(0, -"/index".length);

      // use directory name if component is represented as a directory
      const directory = path.dirname(name);
      const filename = path.basename(name);
      if (directory.endsWith("/" + filename))
        name = name.slice(0, -(filename.length + 1));

      return name;
    },
  });

  return gulp.src("./src/app/**/*.hbs")
    .pipe(render.handlebars({handlebars: handlebars}))
    .pipe(beautify.html())
    .pipe(gulp.dest(outDirectory));
}
gulp.task(pages);

/** Clears NodeJS modules cache. */
function clearNodeCache(done: TaskCallback) {
  const cache = require.cache;
  for (const key of Object.getOwnPropertyNames(cache))
    delete cache[key];

  done();
}
(clearNodeCache as TaskInfo).displayName = "clear-node-cache";

/** Compile scripts. */
const tsProject = ts.createProject("tsconfig.json");
function scripts() {
  return gulp.src("./src/**/*.+(ts|js)")
    .pipe(tsProject())
    .pipe(gulp.dest("./temp"));
}
gulp.task(scripts);

/** Rollup scripts. */
function roll() {
  return gulp.src("./temp/app/js/**/*")
    .pipe(rollupify({
      external: [
        "swiper",
      ],
    }))
    .pipe(gulp.dest(path.join(outDirectory, "js")));
}
gulp.task(roll);

/** Copy images and assets. */
function images() {
  return gulp.src(`${outDirectory}/**/*.+(html|css)`)
    .pipe(resources({base: "src/app"}))
    .pipe(gulp.dest(outDirectory));
}
gulp.task(images);

/** Copy vendor files. */
function vendor() {
  return gulp.src("./src/app/vendor/**/*")
    .pipe(gulp.dest(path.join(outDirectory, "vendor")));
}
gulp.task(vendor);

/** Responsive images. */
function responsive() {
  return gulp.src(`${outDirectory}/**/*.+(html)`)
    .pipe(responsify({base: "src/app"}))
    .pipe(gulp.dest(outDirectory));
}

/** Minify everything. */
function minify() {
  return gulp.src(`${outDirectory}/**/*`)
    .pipe(minifier({
      minify: true,
      minifyHTML: {
        collapseWhitespace: true,
        conservativeCollapse: true,
      },
      minifyJS: {
        sourceMap: !production,
      },
      minifyCSS: true,
    }))
    .pipe(gulp.dest(outDirectory));
}

/** All tasks sequence. */
gulp.task("default", series(
  clean,
  parallel(
    pages,
    style,
    series(
      scripts,
      roll,
    ),
  ),
  parallel(
    responsive,
    images,
  ),
  minify,
));

// watch for changes
function watch() {
  gulp.watch("./src/**/*.hbs", series(pages, responsive));
  gulp.watch("./src/**/*.+(css|scss)", style);
  gulp.watch("./src/**/*.+(ts|js)", series(clearNodeCache, scripts, roll));
  gulp.watch("./src/data/**/*", series(clearNodeCache, pages));
  gulp.watch(`${outDirectory}/**/*.+(html|css)`, images);
}
gulp.task("watch", gulp.series("default", watch));
