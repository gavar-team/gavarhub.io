import beautify from "js-beautify";
import { EOL } from "os";
import { TransformStream } from "../core";

type Beautifier<T> = (text: string, options?: T) => string;

class BeautifyStream<T> extends TransformStream {

  constructor(readonly beautifier: Beautifier<T>,
              readonly options?: T) {
    super();
  }

  /** @inheritDoc */
  transformBuffer(buffer: Buffer): Buffer | Promise<Buffer> {

    // beautify
    let contents = buffer.toString();
    contents = this.beautifier(contents, this.options);

    // apply
    return new Buffer(contents);
  }
}

export function js(options?: JsBeautifyOptions): BeautifyStream<JsBeautifyOptions> {
  const defaults: JsBeautifyOptions = {
    eol: EOL,
    indent_size: 2,
    end_with_newline: true,
    preserve_newlines: false,
  };
  return new BeautifyStream(beautify.js, Object.assign(defaults, options));
}

export function html(options?: HTMLBeautifyOptions): BeautifyStream<HTMLBeautifyOptions> {
  const defaults: HTMLBeautifyOptions = {
    eol: EOL,
    indent_size: 2,
    end_with_newline: true,
    preserve_newlines: false,
  };
  return new BeautifyStream(beautify.html, Object.assign(defaults, options));
}

export function css(options?: CSSBeautifyOptions): BeautifyStream<CSSBeautifyOptions> {
  const defaults: CSSBeautifyOptions = {
    eol: EOL,
    indent_size: 2,
    end_with_newline: true,
  };
  return new BeautifyStream(beautify.css, Object.assign(defaults, options));
}
