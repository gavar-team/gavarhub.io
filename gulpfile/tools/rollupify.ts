import { GlobalsOption, rollup } from "rollup";
import buble from "rollup-plugin-buble";
import commonjs from "rollup-plugin-commonjs";
import resolve from "rollup-plugin-node-resolve";
import { isString } from "util";
import File from "vinyl";
import { TransformStream } from "../core";

interface RollupifyOptions {
  external?: string[],
  globals?: GlobalsOption,
}

class Rollupify extends TransformStream {

  constructor(public readonly options?: RollupifyOptions) {
    super();
    const defaults: RollupifyOptions = {
      external: [],
      globals: {},
    };
    this.options = Object.assign(defaults, options);
  }

  /** @inheritDoc */
  async transformBuffer(buffer: Buffer, file: File): Promise<Buffer> {

    const bundle = await rollup({
      input: file.path,
      external: this.options.external,
      plugins: [
        buble({
          transforms: {
            dangerousForOf: true,
          },
        }) as any,
        resolve({
          jsnext: true,
          main: true,
          customResolveOptions: {
            moduleDirectory: [
              "node_modules",
              "temp",
            ],
          },
        }),
        commonjs({
          include: "node_modules/**",
        }),
      ],
    });

    const {output} = await bundle.generate({
      format: "iife",
      globals: this.options.globals,
    });

    // remove external imports
    let code = output[0].code;
    for (let i = 0; i < this.options.external.length; i++) {
      const entry = this.options.external[i];
      if (isString(entry)) {
        const regex = new RegExp(`import.*from.*'${entry}'.*`, "g");
        code = code.replace(regex, " ");
      }
    }

    buffer = new Buffer(code.trim());
    return buffer;
  }
}

export function rollupify(options?: RollupifyOptions): Rollupify {
  return new Rollupify(options);
}
